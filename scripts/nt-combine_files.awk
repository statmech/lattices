#!/usr/bin/awk -f
#################################
# combine_files.awk <file1> [ <file2> ] ...[ <filen> ]
#
# This script will print the mean of the values from the first row
# of every file, the mean of the values second rows, and so on. 
#
# All the file must have the same number of entries.
#
# If more than one column per file, the rule it will be applied to
# every column individually.
# 
#################################

{


    # Take count of every field and record
    for (column=1; column<=NF; column++)                    
	a[FNR,column] += $column
}
 
# Set n_fields            
NF > n_fields {
    n_fields = NF
}

# Set n_files
FNR == 1 {
    n_files ++
}

END{

    for (row=1; row<=FNR; row++)
    {
	for (column=1; column<n_fields; column++)
	    printf("%f ", (a[row,column]/n_files))

	print a[row,n_fields]/n_files
    }

}
