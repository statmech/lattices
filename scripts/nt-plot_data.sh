#!/bin/bash
# plot_data.sh [options] <file1> [<file2>] ... [<filen>]
#
# This programs plot the data contained on files given by command line.
#
# It displays the image by default. If -o option is given, it saves it to
# a file.
#
# If no -g is specified, <file1> will be the name of the file but with
# extension .gnu instead

LANG=C

usage () 
{
    cat << EOF
Usage: plot_data.sh [options] <file1> [<file2>] ... [<filen>]

This script generates a gnuplot file and execute it.

OPTIONS:
  -h             Show this message.
  -o <filename>  Save graph on <filename> instead of plot it.
  -g <filename>  Name for the gnuplot file. If not given, as <file1>
                 but finished at .gnu .
  -l <axis>      Plot axis <axis> in log scale. <axis> can be x, y or xy.
  -t <title>     Set a title to the graph.
  -x <label>     Set label of axis x.
  -y <label>     Set label of axis y.
  -k <pos>       Set position of the key. <key> can be t, tl, tr, b, bl anf br.
  -i <text>      Write <text> after all filenames to plot
  -1 <text>      Write <text> after filename of the first one to plot
  -0 <text>      Write <text> after all files to plot except the first one
  -f <mode>      Fit according to <mode>. "p" is power-law.
EOF
}


# Check the options
while getopts ":o:g:l:i:1:0:t:x:y:k:f:" option
do
    case $option in
	h)
	    usage
	    exit 0
	    ;;
	o)
	    graph_file=$OPTARG
	    ;;
	g)
	    gnuplot_file=$OPTARG
	    ;;
	l)
	    if   [[ $OPTARG = "x" ]]
	    then
		logx="1"
	    elif [[ $OPTARG = "y" ]]
	    then
		logy="1"
	    elif [[ $OPTARG = "xy" ]]
	    then
		logx="1"
		logy="1"
	    else
		usage
		exit 1
	    fi
	    ;;
	i)
	    first_option=$OPTARG
	    not_first_option=$OPTARG
	    ;;
	1)
	    first_option=$OPTARG
	    ;;
	0)
	    not_first_option=$OPTARG
	    ;;
	t)
	    title=$OPTARG
	    ;;
	x)
	    xlabel=$OPTARG
	    ;;
	y)
	    ylabel=$OPTARG
	    ;;
	k)
	    if   [[ $OPTARG = "b"  ]]; then key_place="bottom"        
	    elif [[ $OPTARG = "bl" ]]; then key_place="bottom left"
	    elif [[ $OPTARG = "br" ]]; then key_place="bottom right"
	    elif [[ $OPTARG = "t"  ]]; then key_place="top"
	    elif [[ $OPTARG = "tl" ]]; then key_place="top left"
	    elif [[ $OPTARG = "tr" ]]; then key_place="top right"
	    else usage
	    fi
	    ;;
	f)
	    if [[ $OPTARG = "p" ]]
	    then
		fit_expr="a*x**b"
		fit_title="(sprintf('%f',a).\"x**\".sprintf('%f',b))"
	    else usage
	    fi
	    ;;
	?)
	    usage
	    exit 1
	    ;;
    esac
done

shift $((OPTIND-1)); OPTIND=1

path=${1%/*}
if [[ $path == $1 ]]
then
    path=""
else
    path=${path}"/"
fi

filename=${1##*/}
filename_base=${filename/.*/}

# Use the first data_file to name gnuplot file, if not specified
if [[ -z $gnuplot_file ]]
then
    gnuplot_file=${filename_base}.gnu
fi

if [[ ! -z $graph_file ]]
then
    # take graph_file until "/"
    graph_path=${graph_file%/*}
    if [[ $graph_path == $graph_file ]]
    then
	# if there is no "/": same directory 
	graph_path="" # the same that "./"
    else
	# if there is, add a "/" at the end
	graph_path=${graph_path}"/"
    fi

    graph_filename=${graph_file##*/}
fi


# Be sure no previous gnuplot_file exists
rm -f $gnuplot_file

# Write the gnuplot file ------------

echo "path=\"${path}\""   >> $gnuplot_file

if [[ ! -z $graph_file ]]
then
    echo "graph_path=\"${graph_path}\""   >> $gnuplot_file    
fi

# Set title and labels, if any
if [[ ! -z $title ]]
then
    echo "set title \"${title}\""   >> $gnuplot_file
fi
if [[ ! -z $xlabel ]]
then
    echo "set xlabel \"${xlabel}\"">> $gnuplot_file
fi
if [[ ! -z $ylabel ]]
then
    echo "set ylabel \"${ylabel}\"" >> $gnuplot_file
fi

# Set position of the key
if [[ -z $key_place ]]
then
    echo "unset key"           >> $gnuplot_file
else
    echo "set key " $key_place >> $gnuplot_file
fi

# Set output terminal
if [[ ! -z $graph_file ]]
then
    echo "set term png" >> $gnuplot_file
    echo 'set output graph_path."'${graph_filename}'"' >> $gnuplot_file
fi

# Set logscale
if [[ ! -z $logx ]]
then
    echo "set logscale x" >> $gnuplot_file
fi
if [[ ! -z $logy ]]
then
    echo "set logscale y" >> $gnuplot_file
fi

if [[ ! -z $fit_expr ]]
then
    printf "fit %s path.\"%s\" via a,b \n" $fit_expr ${1##*/} >> $gnuplot_file
fi

# First file to plot
printf "plot path.\"%s\" %s " ${1##*/} "$first_option" >> $gnuplot_file

# The rest of files to plot
if [[ $# != "1" ]]
then 

    shift 1

    for i in "$@"
    do
	printf "\\" >> $gnuplot_file
	printf "\n" >> $gnuplot_file
	printf "   , path.\"%s\" %s " ${i##*/} "$not_first_option" >> $gnuplot_file
    done

fi


if [[ ! -z $fit_expr ]]
then
    printf "\\" >> $gnuplot_file
    printf "\n" >> $gnuplot_file
    printf "    , %s t %s" $fit_expr $fit_title >> $gnuplot_file
fi


# Print the new endline!
printf "\n" >> $gnuplot_file


if [[ -z $graph_file ]]
then
    echo "Press a key to continue..."
    echo "pause -1" >> $gnuplot_file
fi

# And plot the graph
gnuplot $gnuplot_file

exit 0
