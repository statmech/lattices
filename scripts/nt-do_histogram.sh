#!/bin/bash
#################################
# do_histogram.sh [-o <graph_file_name>] [-a min] [-b max] [-n nbins] <file>
#################################

LANG=C

usage () 
{
    cat << EOF
Usage: do_histogram.sh [options] <file>

This script generates a histogram with gnuplot.

OPTIONS:
  -h             Show this message.
  -o <filename>  Save graph on <filename> instead of plot it.
  -g <filename>  Name for the gnuplot file. If not given, as <file>
                 but finished at .gnu .
  -a <min>       Specify min value. Only considered if -b is also given.
  -b <max>       Specify max value. Only considered if -a is also given.
  -n <number_of_bins>
                 Specify numbers of bins to use. 50 are used by default.
EOF
}

# Check the options
while getopts ":o:a:b:n:" option
do
    case $option in
	h)
	    usage
	    exit 0
	    ;;
	o)
	    graph_file_name=$OPTARG
	    use_output="graph_file_name="$graph_file_name
	    ;;
	g)
	    gnuplot_file=$OPTARG
	    ;;
	a)
	    min=$OPTARG
	    ;;
	b)
	    max=$OPTARG
	    ;;
	n)
	    nbins=$OPTARG
	    ;;
	?)
	    usage
	    exit 1
	    ;;
    esac
done

shift $((OPTIND-1)); OPTIND=1

# Assign the rest of passed arguments
if [[ -z $1 ]]
then
    usage
    exit 1
else
    filename=$1
fi

# Check that one file is given
if [[ -z $filename ]]
then
    usage
    exit 1
fi

# If min and max are not given, use them
if [[ -z $min || -z $max ]]
then

    get_statistics_of_sampling.awk $filename > aux_file
    min=`cat aux_file | awk '/MIN/ {print $2}'`     
    max=`cat aux_file | awk '/MAX/ {print $2}'`     
    rm aux_file

fi

# Add NBINS if asked
if [[ ! -z $nbins ]]
then
    use_nbins="nbins="$nbins
fi


# Define some new filenames
# Remove extension, if any...
filename_base=${filename/.*/}

# ...and define other files
hist_file=${filename_base}.histo

if [[ -z $gnuplot_file ]]
then
    gnuplot_file=${filename_base}.gnu
fi
# Be sure that these does not exist
rm -f $hist_file $gnuplot_file

# Save the gnuplot commands for generate the histogram
print_gnuplot_histogram.awk   \
    "min="${min} "max="${max} \
    $use_nbins                \
    $use_output               \
    $filename > $gnuplot_file

# Plot the histogram
gnuplot $gnuplot_file

# Show some info if graph is being displayed
if [[ -z graph_file_name ]]
then
    echo min: $min
    echo max: $max
fi

exit 0
