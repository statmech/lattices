#!/bin/awk -f
BEGIN {
    prob[0] = 0
}

{
    if (NF>=1 && $1 != "#")
    {
	value[NR] = $1;
	prob[NR]  = $2;
	# it will be useful for knowing number of valid lines out of first comments
	n++
    }
}

END {
    for (i=(NR-1); i>=(NR-n+1); i--) prob[i] += prob[i+1]
    for (i=(NR-n+1); i<NR; i++) printf "%g %.15g\n", value[i], prob[i]
}
