#!/bin/bash
#################################
# collapse_rsa.sh [options] <file1> <file2> [file3] ... [filen]
#
# This script means every single file and defines N
# The area A of the region is taken from the name of the file, which 
# must have the form A_whatever[.foo]
#
# It plots you a ....
#################################

# Avoid problems calling printf
LANG=C

title="Collapsing RSA"
xlabel="n/((<n>)*log(<n>)*(1+(a/2)*log(<n>)))"
ylabel="C(n|A)*log(<n>)*(1+(a/2)*log(<n>))"
key_place="bottom left"

usage ()
{
    cat <<EOF
  Usage: collapse_rsa.sh [options] <file1> [<file2>] ... [<filen>]

  collapse_rsa.sh [options] <file1> <file2> [file3] ... [filen]

  This script means every single file and defines N
  The area A of the region is taken from the name of the file, which 
must
  have the form A_whatever[.foo]

  It plots you a ....

  OPTIONS:
   -h             Show this message.
   -o <filename>  Save graph on <filename> instead of plot it.
   -g <filename>  Name for the gnuplot file. If not given, as <file1>
                  but finished at .gnu .
EOF
}


# Check the options
while getopts ":h:o:g:" option
do
     case $option in
         h)
             usage
             exit 0
             ;;
         o)
             graph_filename=$OPTARG
             ;;
         g)
             gnuplot_filename=$OPTARG
             ;;
         ?)
             usage
             exit 1
             ;;
     esac
done

shift $((OPTIND-1)); OPTIND=1

# RSA files
j=0
for i in *rsa*.norm
do
     filename=$i
     # Get the side
     side=${filename#*_}
     side=${side%%_*}
     # Set side as a key for knowing which j to use
     key[$side]=$j

     # Save the area
     area[${key[$side]}]=$((side*side))
     # Save the filename
     rsa_file[${key[$side]}]=$filename
     # Save the mean
     rsa_mean[${key[$side]}]=`nt-get_statistics_of_pdf.awk $i | awk '/MEAN/ {print $2}'`
     j=$((j+1))
done
n_files=$j


# Diveristiy files
#j=0
#for i in *diversity*.norm
#do
#     filename=$i
#     # Get the side
#     side=${filename%%_*}
#
#     # Save the mean
#     div_mean[${key[$side]}]=`nt-get_statistics_of_pdf.awk $i | awk '/MEAN/ {print# $2}'`
#     # Save filename
#     div_file[${key[$side]}]=$filename
#
#     j=$((j+1))
#done
#if [[ ${n_files} != ${j} ]]
#then
#    echo "There isn't the same number of div files than rsa files."
#    echo ${j}"!="$n_files
#    exit 1
#fi


# RSA cummulated files
j=0
for i in *rsa*.cum
do
     filename=$i
     # Get the side
     side=${filename#*_}
     side=${side%%_*}

     # Save the filename
     rsa_cum_file[${key[$side]}]=$filename

     j=$((j+1))
done
if [[ ${n_files} != ${j} ]]
then
    echo "There isn't the same number of rsa cumulated files than rsa files."
    exit 1
fi

# Set gnuplot if no one is specified
if [[ -z $gnuplot_filename ]]
then
    gnuplot_filename=collapse_rsa.gnu
fi
# Be sure no previous gnuplot_file exists
rm -f $gnuplot_filename


# Write gnuplot file
echo "path=\"./\""   >> $gnuplot_filename
echo "set title \"${title}\""   >> $gnuplot_filename
echo "set xlabel \"${xlabel}\"">> $gnuplot_filename
echo "set ylabel \"${ylabel}\"" >> $gnuplot_filename
echo "set key "${key_place} >> $gnuplot_filename
 # Set output terminal
if [[ ! -z $graph_filename ]]
then
    echo "set term png" >> $gnuplot_filename
    echo 'set output "'${graph_filename}'"' >> $gnuplot_filename
fi
echo "set logscale xy" >> $gnuplot_filename

# Define main vars
printf "\n" >> $gnuplot_filename
printf "a=-0.12\n" >> $gnuplot_filename
printf "\n" >> $gnuplot_filename

for (( j=0; j<n_files; j=j+1 ))
do
    n_name[$j]="n"${j}
    printf "%s=%.15g\n" ${n_name[$j]} ${rsa_mean[$j]} >> $gnuplot_filename
done
printf "\n" >> $gnuplot_filename

# Plot instructions

 # First file to plot
j=0
printf "plot path.\"%s\" \\" ${rsa_cum_file[${j}]} >> $gnuplot_filename
printf "\n" >> $gnuplot_filename
 # $1 is n
printf "  u (\$1/((%s)*log(%s)*(1+(a/2)*log(%s)))) \\" \
    ${n_name[$j]} ${n_name[$j]} ${n_name[$j]} >> $gnuplot_filename
printf "\n" >> $gnuplot_filename
 # $2 is C(n|a)
printf "   :(\$2*log(%s)*(1+(a/2)*log(%s))) \\" \
    ${n_name[$j]} ${n_name[$j]}  >> $gnuplot_filename
printf "\n" >> $gnuplot_filename
#printf "t \"N=%s, S=%s\" w d" ${rsa_mean[${j}]} ${div_mean[${j}]}  >> $gnuplot_filename  
printf "t \"<n>=%s\" w l" ${rsa_mean[${j}]} >> $gnuplot_filename  

for (( j=1; j<n_files; j=j+1 ))
do
    printf "\\" >> $gnuplot_filename
    printf "\n" >> $gnuplot_filename
    printf "\\" >> $gnuplot_filename
    printf "\n" >> $gnuplot_filename

    printf "   , path.\"%s\" \\" ${rsa_cum_file[${j}]} >> $gnuplot_filename
    printf "\n" >> $gnuplot_filename
    # $1 is n
    printf "  u (\$1/((%s)*log(%s)*(1+(a/2)*log(%s)))) \\" \
	${n_name[$j]} ${n_name[$j]} ${n_name[$j]} >> $gnuplot_filename
    printf "\n" >> $gnuplot_filename
    # $2 is C(n|a)
    printf "   :(\$2*log(%s)*(1+(a/2)*log(%s))) \\" \
	${n_name[$j]} ${n_name[$j]}  >> $gnuplot_filename
    printf "\n" >> $gnuplot_filename
#    printf "t \"N=%s, S=%s\" w d" ${rsa_mean[${j}]} ${div_mean[${j}]}  >> $gnuplot_filename  
    printf "t \"<n>=%s\" w l" ${rsa_mean[${j}]} >> $gnuplot_filename  
 done

printf "\n" >> $gnuplot_filename

# Pause

if [[ -z $graph_filename ]]
then
    echo "Press a key to continue..."
    echo "pause -1" >> $gnuplot_filename
fi

gnuplot $gnuplot_filename

exit 0
