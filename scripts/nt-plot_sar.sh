#!/bin/bash

PLOT_SCRIPT=nt-plot_data.sh

$PLOT_SCRIPT \
    -t "SAR" \
    -x "Area"        \
    -y "# species"        \
    -l xy \
    -i "w lp" \
    "$@"
