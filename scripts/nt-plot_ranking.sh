#!/bin/bash

PLOT_SCRIPT=nt-plot_data.sh

$PLOT_SCRIPT \
    -t "Ranking of species" \
    -i "w lp" \
    "$@"
