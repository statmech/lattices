#!/usr/bin/awk -f
{
    if (! match($0, "#")) {
	# Increase statistics of $1 to the total sum given by all the files
	a[$1] += $2
	if (NR == 1)
	{
	    min1 = $1
	    max1 = $1
	}
        # Set max and min
	if ($1 < min1)  min1 = $1
	if ($1 > max1)  max1 = $1
    }
}


END{

    for (x=min1; x<=max1; x++)
    {
	if (a[x] != 0) print x, a[x]
    }

}
