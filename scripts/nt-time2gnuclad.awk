#!/bin/awk -f

# script.awk <file>

# INPUT:
# This script understands <file> as a comma separated value file.
# The 5th field of every line must be a negative number.

# OUTPUT:
# This script send stdout a copy <file> but the 5th field in logscale,
# and additionally corrected in such a way that is understood by gnuclad.

# The following illustrate the map of correction. The correction, is
# unrelated to the logscale, it just applies over negative numbers.

# ******************************* MAP ****************************************
#
# R_:
#     -(n+1).00    -n.99     -n.98 -n.03   -n.02     -n.01     -n.00 -(n-1).99
#           |         |         |   |         |         |          |      |
#-[---------[---------[---------[~~~[---------[---------[----------[------[--
# clap:     |         |         |   |         |         |          |      |
# -(n+2).100| -(n+1).1|-(n+1).2 |   |-(n+1).98|-(n+1).99|-(n+1).100| -n.1 |
#           |         |         |   |         |         |          |      |
# rest:     |         |         |   |         |         |          |      |
#         0.00       0.01     0.02 0.97      0.98      0.99       0.00   0.01
#
# ****************************************************************************

# R_   values stand for points from the negative part of the Real axis.
# clap values stand for values associated to those points on the interval
#             right-closed and left-opened defined by the nearest R_ values.
# rest values stand for the result of the operation (x+0.01)%1, the python way.
#              For negative numbers, it's equivalent to 1+(x+0.01)-int(x+0.01).

BEGIN {
    # Use comma as separator
    FS=",";
    OFS=",";
    prec = 0.01; # Don't change without caring about the format of printf!
}
{
    # Logscale of a negative value
    $5 = -log(-$5);
    printf "%s,%s,%s,%s,", $1, $2, $3, $4;
    # Unique exception: if $5 is very near to 0, it is 0
    # (It helps to -log(-1) imprecissions and is eye-candy)
    if ($5 > 0-prec) printf "%010.2f", 0.;
    else
    {
	# Implement the map of above
	rest = ($5 + prec) + 1 - int($5+prec);
	if (rest >= 1-prec)
	    printf "%010.3f", -1+int($5)-0.1;
	else
	{
	    if (rest < prec)
		printf "%09.2f", int($5)-prec;
	    else
		printf "%09.2f", -1+int($5)-rest;
	}
    }
    printf ",,,,\n";
}
