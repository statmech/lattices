#!/bin/bash

PLOT_SCRIPT=nt-plot_data.sh

$PLOT_SCRIPT \
    -t "Species diversity" \
    -x "#species"        \
    -i "w l" \
    "$@"
