#!/usr/bin/awk -f
BEGIN {
    n = 0;
    sumy = 0;
    sumweight = 0;
    mean = 0;
    m2 = 0;
    first_count = 1;
}

{
    if (! match($0, "#")){
	n++;
	sumy += $2;
	# Calculate mean and variance
	temp = $2 + sumweight;
	delta = $1 - mean;
	r = delta * $2 / temp;
	mean += r;
        m2 += sumweight * delta * r;
	sumweight = temp;

	if (first_count == 1)
	{
	    max=$1;
	    min=$1;
	    first_count = 0;
	}
	if ($1 > max) max = $1;
	if ($1 < min) min = $1;
    }
}

END {
    printf "N %g\n", n;
    printf "SUM %g\n", sumy;
    printf "MEAN %.15g\n", mean;
    variance_n = m2/sumweight;
    printf "VARIANCE_N %.15g\n", variance_n;
    variance = variance_n * n/(n+1);
    printf "VARIANCE %.15g\n", variance;
    printf "SIGMA_N %.15g\n", sqrt(variance);
    printf "SIGMA %.15g\n", sqrt(variance_n);
    printf "MIN %g\n", min;
    printf "MAX %g\n", max;
}
