#!/bin/bash
norm=`nt-get_statistics_of_pdf.awk $1 | awk '/SUM/ {print $2}'`
awk '{if (! match($0, "#")){printf "%.15g %.15g\n", $1, $2/norm}}' norm=$norm < ${1}
