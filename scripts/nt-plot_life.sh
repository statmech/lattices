#!/bin/bash

PLOT_SCRIPT=nt-plot_data.sh

$PLOT_SCRIPT \
    -t "Time of life" \
    -x "t"        \
    -l xy \
    "$@"
