#!/bin/bash
#################################
# dim_files2plot.sh [-o graph_filename] <file1> <file2> [file3] ... [filen]
#
# This script means every single file and represents its
# means against the dimension indicated in the name of every file.
#
# The dimension is taken from the name of the file, which must
# have the form 
#   whatever_dim[.foo]
# dim is the dimension value and must be between the last underscore
# and the end of the filename or the extension.
#################################

LANG=C

usage () 
{
    cat << EOF
Usage: dim_files2plot.sh [options] <file1> [<file2>] ... [<filen>]

 This script means every single file and represents its means against
 the dimension.

 The dimension is taken from the name of the file, which must
 have the form 
   whatever_dim[.foo]
 where dim is that value. It must be placed between the last underscore
 and the end of the filename or the extension.

OPTIONS:
  -h             Show this message.
  -o <filename>  Save graph on <filename> instead of plot it.
  -g <filename>  Name for the gnuplot file. If not given, as <file1>
                 but finished at .gnu .
EOF
}


# Check the options
while getopts ":h:o:g:" option
do
    case $option in
	h)
	    usage
	    exit 0
	    ;;
	o)
	    graph_filename=$OPTARG
	    ;;
	g)
	    gnuplot_filename=$OPTARG
	    ;;
	?)
	    usage
	    exit 1
	    ;;
    esac
done

shift $((OPTIND-1)); OPTIND=1

if [[ -z $1 ]]
then
    usage
    exit 1
fi    

filename_base=${1/.*/}

if [[ -z $gnuplot_file ]]
then
    gnuplot_file=${filename_base}.gnu
fi

# Stablish filenames which will be created
means_filename="voter_model_dim-time.data"
gnuplot_filename="voter_model_dim-time.gnu"

# Be sure that these does not exist
rm -f $means_filename $gnuplot_filename

for i in "$@"
do

    filename=$i
    # Get the dimension
    dim=${filename##*_}
    # Avoid file extension, if any
    dim=${dim/.*/}
    # Get the mean
    mean=`get_statistics_of_sampling.awk $i | awk '/MEAN/ {print $2}'` 
    # Write info on a file
    echo $dim $mean >> $means_filename

done

# Sort data
sort -n $means_filename > sorted.aux
mv sorted.aux $means_filename

# Write the gnuplot file
if [[ -z $graph_filename ]]
then
    plot_data.sh                    -g $gnuplot_filename -l xy -i "w lines" $means_filename
else
    plot_data.sh -o $graph_filename -g $gnuplot_filename -l xy -i "w lines" $means_filename
fi

exit 0
