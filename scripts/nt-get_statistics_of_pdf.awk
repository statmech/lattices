#!/usr/bin/awk -f

# It is expecting records of type:
#
#    left_value    right_value    probability
#
BEGIN {
    n = 0;
    sumy = 0;
    W = 0;
    mean = 0;
    m2 = 0;
    first_count = 1;
}

{
    if (! match($0, "#")){
	n++;
	x_left  = $1;
	x_right = $2;
	prob    = $3;

	# The weight of every bin is prop to width and probability
	x  = (x_right + x_left)/2.;
	weight = (x_right-x_left) * prob;

	sumy += weight;

	# Calculate mean and variance
	temp = weight + W;
	delta = x - mean;
	r = delta * weight / temp;
	mean += r;
        m2 += W * delta * r;
	W = temp;

	if (first_count == 1)
	{
	    max=$1;
	    min=$1;
	    first_count = 0;
	}
	if ($1 > max) max = $1;
	if ($1 < min) min = $1;
    }
}

END {
    printf "N %g\n", n;
    printf "SUM %.15g\n", sumy;
    printf "MEAN %.15g\n", mean;
    variance_n = m2/W;
    printf "VARIANCE_N %.15g\n", variance_n;
    variance = variance_n * n/(n+1);
    printf "VARIANCE %.15g\n", variance;
    printf "SIGMA_N %.15g\n", sqrt(variance);
    printf "SIGMA %.15g\n", sqrt(variance_n);
    printf "LEFT %g\n", min;
    printf "RIGHT %g\n", max;
}
