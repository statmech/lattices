#!/bin/bash

PLOT_SCRIPT=nt-plot_data.sh

$PLOT_SCRIPT \
    -t "SAD" \
    -x "# individuals"        \
    -y "# species"        \
    -l xy \
    -i "w l" \
    "$@"
