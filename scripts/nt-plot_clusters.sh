#!/bin/bash

PLOT_SCRIPT=nt-plot_data.sh

$PLOT_SCRIPT \
    -t "Size of clusters" \
    -x "# individuals"        \
    -l xy \
    -i "w lp" \
    "$@"
