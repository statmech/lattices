#!/bin/awk -f
#############################################
# LOG-BINNING
#
# It is expected that data is sorted!
#############################################
BEGIN {
    # Width: Change it from command line
    width = 1.2;

    # Internal vars
    previous_bin_index = -1;
    total_x = 0.;
    total_y = 0.;
    n = 0;
    first_valid_line = 1;
}

{
    # Count only those points inside the range to plot
    if (NF>=1 && $1 != "#")
    {
	if (first_valid_line == 1)
	{
	    previous_bin_index = int( log($1)/log(width) );
	    first_valid_line = 0;
	}
	# Add a count for the correct bin
	# (int is flooring, so histo[i] starts at i=0)
	bin_index = int( log($1)/log(width) )
	if (previous_bin_index != bin_index)
	{
	    printf ("%f %.15g\n", total_x/n, total_y/n);
	    # restart vars
	    total_x = 0.;
	    total_y = 0.;
	    previous_bin_index = bin_index;
	    n = 0;
	}
	# How many points did you take it on this bin?
	n++;
	# Which are the total sum of the coordinates on this bin?
	total_x += $1;
	total_y += $2;
    }
}

END{printf ("%f %.15g\n", total_x/n, total_y/n)}










