#!/bin/bash

PLOT_SCRIPT=nt-plot_data.sh

$PLOT_SCRIPT \
    -t "Beta Diversity" \
    -x "distance"        \
    -y "correlation"        \
    -l x \
    -i "w lp" \
    "$@"
