#!/bin/awk -f
#
# print_gnuplot_histogram.awk min=<min_value> max=<max_value>
#                             [ nbins=<value1> ] [ graph_file_name=<value2> ]
#                             <data_file>
#
# This script expects a datafile of a single entry and will return
# needed gnuplot's commands for printing an histogram.
#
# If option "nbins" is not given, value 50 will be used.
# If option "graph_file_name" is given, gnuplot commands will be such that
#  they will save the plot on a file and will not display it at the screen.

BEGIN {

    # Assign defaults if needed
    nbins = 50;

}

{

    if (NR == 1)
    {

	# Check that needed variables are defined
	if (min == "") {print "Specify min value!"; exit};
	if (max == "") {print "Specify max value!"; exit};

	# Calculate width of the bins
	bin_width = (max - min)/nbins;

	# Change gnuplot term if indicated
	if (graph_file_name != "") 
	{
	    print "set term png";
	    print "set output \"" graph_file_name "\"";
	};
    
	# Print gnuplot settings
	print "unset key";
	print "set xrange [", min, ":", max, "]";
#	print "set logscale x"
	print "set boxwidth", bin_width, "absolute";
	print "set style fill solid 1.00 border -1";
	print "plot '-' w boxes lc rgb\"green\" ";

    }

    # Count only those points inside the range to plot
    if (NF == 1 && $1 > min && $1 < max)
	# Add a count for the correct bin
	# (int is flooring, so histo[i] starts at i=0)
	histo[ int( ($1-min)/bin_width) ]++;
}

END {

    # Check, again, that needed variables are defined
    if (min == "") exit;
    if (max == "") exit;

    # Print all the info we got
    for (i in histo) print min + i*bin_width, histo[i];
    print "e";

    # Pause gnuplot if it is intended to display the graph
    if (graph_file_name == "") print "pause -1";
}












