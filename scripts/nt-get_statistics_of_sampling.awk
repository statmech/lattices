#!/usr/bin/awk -f
BEGIN {
    n   = 0;
    sum = 0;
    min = 0;
    max = 0;
    mean = 0;
    m2 = 0;
    first_count = 1;
}

{
    if (! match($0, "#")){
	n++;
	sum += $1;
	# Calculate mean and variance
	delta = $1 - mean;
	mean += delta / n;
	m2 += delta*($1-mean);
	if (first_count == 1)
	{
	    max=$1;
	    min=$1;
	    first_count = 0
	}
	if ($1 > max) max = $1;
	if ($1 < min) min = $1;
    }
}

END {
    printf "N %d\n",    n;
    printf "SUM %.15g\n",  sum;
    printf "MEAN %.15g\n", mean;
    variance = m2/(n-1);
    printf "VARIANCE %.15g\n", variance;
    printf "SIGMA %.15g\n", sqrt(variance);
    printf "MIN %.15g\n",  min;
    printf "MAX %.15g\n",  max
}
