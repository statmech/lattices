!#include ../src/parse/getopt.F90

program getopts_test

  use getopt_module

  implicit none

  character(len=100)  :: options
  type(option)        :: longopts(:)
  character           :: optchar
  character (len=500) :: optarg
  integer             :: arglen, stat, offset, remain
  logical             :: err = .FALSE.
  
  options = "hv"
  longopts = option("help", .FALSE., "h", "Print this help", "")

  do
     call getopt(options, longopts, optchar, optarg, arglen, &
          stat, offset, remain, err)
     if (stat /= 0) then
        print *, "Stat is not 0. Exiting..."
        exit
     endif
     select case (optchar)
        case ("h")
           print *, "It was parsed character: ", optchar
     end select
  enddo

end program getopts_test
