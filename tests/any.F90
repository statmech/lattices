          program test_any
            logical l
            l = any((/.true., .true., .true./))
            print *, l
            call section

            PAUSE

            contains
              subroutine section
                integer a(5,2)
                a = 1
                a(2,1) = 2
                a(2,2) = 4
                print *, any(a .eq. 2)
!                print *, any(a .eq. (/ 2, 4 /), 2)
              end subroutine section
          end program test_any
