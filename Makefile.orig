SHELL=/bin/sh

SCRIPTS = ./scripts
CURRENT_DIR = .

FC = gfortran
FCFLAGS = -Ofast -march=native -pipe
CPPFLAGS = -DBINDIR=\"$(SCRIPTS)\" -DPKGDATADIR=\"$(CURRENT_DIR)\"
#FCFLAGS = -ggdb -Wall #-fprofile-arcs -ftest-coverage
# For old versions of gfortran
#FCFLAGS = -O3 -march=native -pipe

# Dependency files
getopt_code=src/getopt.F90
getopt_object=src/getopt.o
plot_lattice_code=src/plot_lattice.F90
plot_lattice_object=src/plot_lattice.o
rng_code=src/ranxor.f

# Main files
voter_model_code=src/voter_model.F90
classic_code=src/forward.F90
coalescent_code=src/coalescent.F90

all: voter_model nt-forward nt-coalescent neutral_theory # neutral_theory-coalescent-pure 

$(getopt_object): $(getopt_code)
	$(FC) -c $(getopt_code) -o $(getopt_object) $(CPPFLAGS) $(FCFLAGS)

$(plot_lattice_object): $(plot_lattice_code)
	$(FC) -c $(plot_lattice_code) -o $(plot_lattice_object) $(CPPFLAGS) $(FCFLAGS)

voter_model: $(plot_lattice_object) $(voter_model_code)
	$(FC) \
             $(rng_code) \
             $(plot_lattice_object)   \
             $(voter_model_code) \
             -o voter_model $(CPPFLAGS) $(FCFLAGS)

nt-forward: $(getopt_object) $(plot_lattice_object) src/forward* src/measuring* src/save_results.F90

	$(FC) \
              $(rng_code) \
              $(plot_lattice_object)   \
              $(getopt_object) \
              $(classic_code) \
              -o nt-forward $(CPPFLAGS) $(FCFLAGS)

nt-coalescent: $(getopt_object) $(plot_lattice_object) src/coalescent* src/measuring* src/save_results.F90
	$(FC) \
              $(rng_code) \
              $(plot_lattice_object)   \
              $(getopt_object) \
              $(coalescent_code) \
              -o nt-coalescent $(CPPFLAGS) $(FCFLAGS)


neutral_theory: nt-forward
	cp src/neutral_theory.sh neutral_theory

clean:
	rm -rf voter_model neutral_theory nt_forward nt_coalescent $(getopt_object) $(plot_lattice_object) getopt_module.mod plot_lattice_mod.mod
