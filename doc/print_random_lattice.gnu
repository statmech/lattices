reset
n=20   #number of rectangles
classes=3 
dx=1.0/n    #legth of rectangle
set xrange [0:n]
set yrange [0:n]
set palette defined(0"white",classes"black")  #set gradient color
set cbrange [0:classes]
unset colorbox
set for [i=1:n] \
for [j=1:n] object (j-1)*n+i rectangle from screen (i-1)*dx,(j-1)*dx to \
screen i*dx+dx/2,j*dx+dx/2 fs solid 1.0 noborder fillcolor palette cb int(rand(0)*classes) \
behind

#draw a set of rectangle with diffrent color."+dx/2" makes the
#adjacent rectangles have a common part, so that the border is not
#so obvious
#set term png
#set output "backgradient1.png"
#plot sin(x) w l lw 0 lc palette cb 0 notitle,sin(x) w l lw 2 lc \
#rgb"blue" notitle

plot 0 w l lw 0 lc palette cb 0 notitle

#The first plot is just used to enable the palette, and it will be
#covered by the second plot never appearing on the output picture.
#set output
pause -1
