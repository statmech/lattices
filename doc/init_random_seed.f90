!SUBROUTINE init_random_seed()
!  INTEGER :: i, n, clock
!  INTEGER, DIMENSION(:), ALLOCATABLE :: seed
!  
!  CALL RANDOM_SEED(size = n)
!  ALLOCATE(seed(n))
!  
!  CALL SYSTEM_CLOCK(COUNT=clock)
!  
!  seed = clock + 37 * (/ (i - 1, i = 1, n) /)
!  !CALL RANDOM_SEED(PUT = seed)
!  CALL ran_ini (seed)
!  
!  DEALLOCATE(seed)
!END SUBROUTINE init_random_seed

PROGRAM test

!DO i=1,10000

  !CALL init_random_seed ()
  !CALL RANDOM_NUMBER (foo)
  CALL init_seed_with_clock ()
  PRINT *, ran_u ()

!ENDDO

END PROGRAM test
