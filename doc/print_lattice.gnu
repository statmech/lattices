# I know the coordinates of the lattice
# i is row and j is collumn
# indexes must start at 1
#

n=20
classes=15

unset key
#unset colorbox

set xrange [0:1]
set yrange [0:n]


# HSV
#set palette model HSV
#set palette defined ( 0 0 1 0, 1 0 1 1, 6 0.8333 1 1, 7 0.8333 0 1)

set palette maxcolors classes
set cbrange [1:classes]

#set palette defined(1"white",4"#ccffcc")  #set gradient color
#set cbrange [1:4]

set for [i=1:n] object i rect from 0,i-1 to 1,i fc palette cb i behind
#set object 1 rect from screen 0,0 to 1 fc palette cb -1
plot 0 lc palette cb 1
pause -1




