! "Pedir" el tamaño de la red
! "Pedir" el valor de la probabilidad 

! Una variable k controla el número de vecinos aleatorios ("el módulo")

! Inicializar la matriz de nodos
! Inicializar los vecinos adyacentes

! Preparao el punto de crecimiento (le digo dónde comenzar)

! Llamar a una subrutina que calcula los vecinos adyacentes de los puntos de crecimiento

  !Sólo pasarle el nuevo punto de crecimientos, que no tenga que determinarlo ella

!Si k=0 -> final

!Subrutina que elige un vecino adyacente y determina aleatoriamente su estado
! Es decir, elige un número aleatorio entre 0 y k

!Según el programa del profesor hay que referenciar tanto la lista de vecinos adyacentes como a las coordenadas de esos vecinos adyacentes.

! Eso de ir cambiando el listado de vecinos disponibles

    !Escoge el correspondiente al k que se había decidido
    !Se cambia sus estado en la matriz
    !El último se pone en su lugar
    !El último se reinicia a 0

!Llamar a la subrutina solo si se enciende la terminal ?¿

!nq=(nex-1)*(L-nex)*(ney-1)*(L-ney) Si eso da cero es que ha llegado o no (!)
  !Sin eso acabará cuando no tenga vecinos (lo del 1 es por que su red es (1:L)x(1-L)


! En realidad su lista de adyacentes contiene las coordenadas del adyacente

! La subrutina adyacentes comprueba el estado de los cuatro puntos cardinales y SOLO actúa en el caso de que el ordenador esté en estado
PROGRAM PERCOLACION
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! Estados: 0 -> Apagado                                                         !!
!!!          1 -> Encendido                                                       !!
!!!          2 -> Futuro                                                          !!
!!!                                                                               !!
!!! La red tendrá l unidades de longitud (l+1 nodos)                              !!
!!!                                                                               !!
!!!                                                                               !!
   INTEGER, PARAMETER :: l = 100 !<- Tamaño de la red                             !!
   REAL,    PARAMETER :: p = 0.55!<- Probabilidad de que el ordenador SÍ funcione !!
!!!                                                                               !!
!!!                                                                               !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
INTEGER :: universo (0:l,0:l)    & !Universo, guarda estados
          ,adyacentes(2,1:l*l)   & !Adyacentes, guarda coordenadas de Universo
          ,posx                  &
          ,posy                  &
          ,arriba                &
          ,abajo                 &
          ,izquierda             &
          ,derecha
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Empieza el programa

! 0. Inicialización
CALL ran_ini(365441)

DO I=1,30

 universo                =           2
 universo  ((l/2),(l/2)) =           1
 adyacentes(  :  ,  1  ) = (/ l/2 -1 , l/2    /)
 adyacentes(  :  ,  2  ) = (/ l/2 +1 , l/2    /)
 adyacentes(  :  ,  3  ) = (/ l/2    , l/2 -1 /)
 adyacentes(  :  ,  4  ) = (/ l/2    , l/2 +1 /)
 k                       =           4



 WRITE(*,*) "#erase"

 DO

 ! 1. Elije un adyacente para ser evaluado, le pone el último encima y reduce k una unidad

  m               = i_ran(k)
  posx            = adyacentes(1,m)
  posy            = adyacentes(2,m)
  adyacentes(:,m) = adyacentes(:,k)

  k = k - 1


  IF (p < ran_u()) THEN ! Si resulta APAGADO

     universo(posx,posy) = 0
     WRITE(*,*) posx, posy, 0

  ELSE                  ! Si resulta ENCENDIDO: renueva adyacentes

     universo(posx,posy) = 1
     WRITE(*,*) posx, posy, 1

     izquierda           = posx-1
     derecha             = posx+1
     arriba              = posy+1
     abajo               = posy-1

     IF (universo(izquierda,posy) == 2)  THEN 

         k = k + 1
         adyacentes(:,k)     = (/izquierda,posy/)

     ENDIF

     IF (universo(derecha,posy) == 2) THEN

         k = k + 1
         adyacentes(:,k)        = (/derecha,posy/)

     ENDIF

     IF (universo(posx,arriba)  == 2) THEN 

         k = k + 1
         adyacentes(:,k)        = (/posx,arriba/)

     ENDIF

     IF (universo(posx,abajo)        == 2) THEN 

         k = k + 1
         adyacentes(:,k)        = (/posx,abajo/)

     ENDIF

  ENDIF

  !WRITE(*,*) "#erase"
  !DO I=0,l
  !DO J=0,l
  !WRITE(*,*) I,J,universo(I,J)/2.
  !ENDDO
  !ENDDO   

  !El programa acaba al final de la red o bloqueado
  IF (k==0.or.posx*posy*(posx-l)*(posy-l)==0) EXIT

 ENDDO

ENDDO

ENDPROGRAM PERCOLACION

