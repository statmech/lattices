  ! Description:
  !
  ! This program reproduces the voter model standing on the following notes:
  !  * Square lattice.
  !  * Every iteration, a random point of the lattice is deleted.
  !  * That point takes the value of one neighbour (with equal probability) 
  !
  ! This method is extensible to more considered neighbours;
  ! however, only 4 and 8 have been coded until now.
  ! (set variable "neighbours" for selecting one of the coded options)

MODULE lattice

  ! Set this important variables
  INTEGER, PARAMETER :: board_side = 30         ! Dimensions of the board
  INTEGER, PARAMETER :: neighbours = 8          ! 4 or 8

  ! Internal variables --------------------

  INTEGER            :: board(board_side, board_side) ! The board
!  INTEGER            :: i_ran                         ! Functions
!  REAL               :: check_density                 ! Functions
END MODULE lattice

PROGRAM VOTER_MODEL

  ! Use the global variables defined above
  USE lattice

!  IMPLICIT NONE

  INTEGER            :: end_of_the_world = 10000       ! Last step

  ! Internal variable definitions
  INTEGER            :: i, j ! Chosen node (i, j)     ! The random state
  INTEGER            :: members                       ! Number of specimens of the first type
  INTEGER            :: magic_n                       ! A magic number
!  INTEGER            :: k, l, m                       ! Dummy indices

  !===========================================================================
  ! Note: "magic_n" is used for assigning a neighbour to the current state.
  !       This takes a random value from 1 to # of neighbours, and then
  !       it is mapped into some neigbour through a "hardcoded" CASE.
  !
  !       Adventages: * Trivial extensibility of the number of neighbours
  !                   * You will go out of the CASE quickly:
  !                     If only n neighbours, one will be assigned before
  !                     reaching the n+1 CASE, although other CASES are written
  !                     for other elections of "neighbours"
  !============================================================================

  ! Start the ranxor generator
  CALL init_seed_with_clock()

  ! Fill randomly the lattice (by default, arrays start at 1)
  members = 0
  DO k = 1, board_side
     DO l = 1, board_side
        board(k,l) = i_ran(2)
        IF (board(k,l) == 0) THEN
           members = members + 1  
        ENDIF
     ENDDO
  ENDDO

  board = (/  (k+l, k=1, board_side), l=1, board_side )  /)

  ! Show initial density on the screen
  PRINT *, members/(1.0 * board_dims * board_dims)

  ! Start the iterations of our lattice
  DO !m = 1, end_of_the_world

     ! CHOOSE A RANDOM POINT
     !
     i =  i_ran(board_side)
     j =  i_ran(board_side)


     ! CHANGE CURRENT VALUE, CONSIDERING THE LIMITS OF THE LATTICE

     ! Reduce one number of specimes (one node must die)
     IF (board(i,j) == 0) THEN
        members = members - 1
     ENDIF

     ! Most probable case: Not on a border
     IF (i /= 1 .AND. i /= board_side .AND. j /= 1 .AND. j /= board_side) THEN
        magic_n = i_ran(neighbours)

        SELECT CASE (magic_n)

           ! Diagram of chosen coordinates
           !
           !   5 1 6
           !   4 x 2
           !   8 3 7

           CASE (1)
              board(i,j) = _UP_

           CASE (2)
              board(i,j) = _RIGHT_

           CASE (3)
              board(i,j) = _DOWN_

           CASE (4)
              board(i,j) = _LEFT_

           CASE (5)
              board(i,j) = _UP_LEFT_

           CASE (6)
              board(i,j) = _UP_RIGHT_

           CASE (7)
              board(i,j) = _DOWN_RIGHT_

           CASE (8)
              board(i,j) = _DOWN_LEFT_

           END SELECT

        ! ------------------------------ Strange case: top side
        ELSE IF (j == 1 .AND. i /= 1 .AND. i /= board_side) THEN

           IF (neighbours == 4) THEN
              magic_n = i_ran(3)
           ELSE ! IF (neigbours = 8)
              magic_n = i_ran(5)
           ENDIF
           
           SELECT CASE (magic_n)

              ! Diagram of chosen coordinates
              !
              !   --------
              !    3 x 1  
              !    5 2 4

           CASE (1)
              board(i,j) = _RIGHT_     

           CASE (2)
              board(i,j) = _DOWN_
        
           CASE (3)
              board(i,j) = _LEFT_

           CASE (4)
              board(i,j) = _DOWN_RIGHT_

           CASE (5)
              board(i,j) = _DOWN_LEFT_

        
           END SELECT

        ! ------------------------------ Strange case: bottom side
        ELSE IF (j == board_side .AND. i /= 1 .AND. i /= board_side) THEN

           IF (neighbours == 4) THEN
              magic_n = i_ran(3)
           ELSE ! IF (neigbours = 8)
              magic_n = i_ran(5)
           ENDIF
           
           SELECT CASE (magic_n)

              ! Diagram of chosen coordinates
              !
              !    4 1 5
              !    3 x 2  
              !   -------

           CASE (1)
              board(i,j) = _UP_

           CASE (2)
              board(i,j) = _RIGHT_
        
           CASE (3)
              board(i,j) = _LEFT_

           CASE (4)
              board(i,j) = _UP_LEFT_

           CASE (5)
              board(i,j) = _UP_RIGHT_

        
           END SELECT

        ! ------------------------------ Strange case: left side
        ELSE IF (i == 1 .AND. j /= 1 .AND. j /= board_side) THEN

           IF (neighbours == 4) THEN
              magic_n = i_ran(3)
           ELSE ! IF (neigbours = 8)
              magic_n = i_ran(5)
           ENDIF
           
           SELECT CASE (magic_n)

              ! Diagram of chosen coordinates
              !
              !    | 1 4
              !    | x 2  
              !    | 3 5

           CASE (1)
              board(i,j) = _UP_

           CASE (2)
              board(i,j) = _RIGHT_
        
           CASE (3)
              board(i,j) = _DOWN_

           CASE (4)
              board(i,j) = _UP_RIGHT_

           CASE (5)
              board(i,j) = _DOWN_RIGHT_ 

        
           END SELECT

        ! ------------------------------ Strange case: right side
        ELSE IF (i == board_side .AND. j /= 1 .AND. j /= board_side) THEN

           IF (neighbours == 4) THEN
              magic_n = i_ran(3)
           ELSE ! IF (neigbours = 8)
              magic_n = i_ran(5)
           ENDIF
           
           SELECT CASE (magic_n)

              ! Diagram of chosen coordinates
              !
              !    5 1 |
              !    3 x |  
              !    4 2 |

           CASE (1)
              board(i,j) = _UP_          

           CASE (2)
              board(i,j) = _DOWN_
        
           CASE (3)
              board(i,j) = _LEFT_

           CASE (4)
              board(i,j) = _DOWN_LEFT_

           CASE (5)
              board(i,j) = _UP_LEFT_           

        
           END SELECT

        !--------------------------------- Very strange case: top left corner 
        ELSE IF (i == 1 .AND. j == 1) THEN

           IF (neighbours == 4) THEN
              magic_n = i_ran(2)
           ELSE ! IF (neigbours = 8)
              magic_n = i_ran(3)
           ENDIF

           
           SELECT CASE (magic_n)

           ! Diagram of chosen coordinates
           !
           !   -----
           !   |x 1
           !   |2 3      

           CASE (1)
              board(i,j) = _RIGHT_           

           CASE (2)
              board(i,j) = _DOWN_
        
           CASE (3)
              board(i,j) = _DOWN_RIGHT_
        
           END SELECT

        !--------------------------------- Very strange case: top right corner 
        ELSE IF (i == 1 .AND. j == board_side) THEN

           IF (neighbours == 4) THEN
              magic_n = i_ran(2)
           ELSE ! IF (neigbours = 8)
              magic_n = i_ran(3)
           ENDIF

           
           SELECT CASE (magic_n)

           ! Diagram of chosen coordinates
           !
           !   -----
           !   2 x |
           !   3 1 |      

           CASE (1)
              board(i,j) = _DOWN_           

           CASE (2)
              board(i,j) = _LEFT_
        
           CASE (3)
              board(i,j) = _DOWN_LEFT_           
        
           END SELECT


        ! ------------------------------ Very strange case: bottom right corner
        ELSE IF (i == board_side .AND. j == board_side) THEN

           IF (neighbours == 4) THEN
              magic_n = i_ran(2)
           ELSE ! IF (neigbours = 8)
              magic_n = i_ran(3)
           ENDIF
           
           SELECT CASE (magic_n)

              ! Diagram of chosen coordinates
              !
              !     3 2 |
              !     1 x |
              !   ------|      

           CASE (1)
              board(i,j) = _LEFT_           

           CASE (2)
              board(i,j) = _UP_
        
           CASE (3)
              board(i,j) = _UP_LEFT_           
        
           END SELECT

        ! ------------------------------ Very strange case: bottom left corner
        ELSE IF (i == board_side .AND. j == 1) THEN

           IF (neighbours == 4) THEN
              magic_n = i_ran(2)
           ELSE ! IF (neigbours = 8)
              magic_n = i_ran(3)
           ENDIF
           
           SELECT CASE (magic_n)

              ! Diagram of chosen coordinates
              !
              !   | 1 3
              !   | x 2 
              !   ------      

           CASE (1)
              board(i,j) = _UP_           

           CASE (2)
              board(i,j) = _RIGHT_
        
           CASE (3)
              board(i,j) = _UP_RIGHT_           
        
           END SELECT


        ! You will not enter here if the code of above is OK
        ELSE
           PRINT *, 'Internal error: Impossible to recognize barriers around node.'
           PRINT *, 'Coordinates: i= ', i, ' j=', j, ' of a square lattice of side=', board_side 
           STOP
        ENDIF


        ! Update alive members, if node really changed
        IF (board(i,j) /= died_member_class) THEN
           IF (died_member_class == 0) THEN
              members = members - 1
           ELSE
              members = members + 1
           ENDIF
        ENDIF

        PRINT *, members/(1.0 * board_dims * board_dims)


        ! Go out if game finished
        IF (members == 0 .OR. members == 1) THEN
           PRINT *, 'Game over.'
           EXIT
        ENDIF
!        CALL print_board

     ENDDO


END PROGRAM VOTER_MODEL

! This subroutine returns the density of the state 1 on the board
! IT HAS NO SENSE NOW!!
REAL FUNCTION count_density ()

  USE lattice

!  IMPLICIT NONE

  INTEGER :: density

  density=0

  DO k = 1, board_side
     DO l = 1, board_side
        IF (board(k,l) == 1) THEN
           density = density + 1
        ENDIF
     ENDDO
  ENDDO

 count_density = density/(1.0 * board_side * board_side)

END FUNCTION count_density
