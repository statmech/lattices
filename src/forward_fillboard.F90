subroutine fill_board ()
  implicit none
  if (init_min_diversity) members_array_size = 50 ! big enough
  if (init_max_diversity) members_array_size = board_side_square

  allocate(members(members_array_size))
  allocate(birthdays(members_array_size))
  allocate(classes_found(members_array_size))
  ! Set initial conditions
  ! All nodes have class 1
  if (init_min_diversity) then
     members         = 0
     ! We start members at 2 instead of 1 for getting better info when plotting
     members(2)      = board_side_square
     birthdays(2)    = 0
     ! board(x,y) is the index of members array which takes count of its class
     board           = 2
     total_n_classes = 1
  endif
  if (init_max_diversity) then
     ! All nodes have a different class, from 1 to board_side_square
     members         = 1
     birthdays       = 0
     block 
       integer :: i, j
       forall (i=1:board_side, j=1:board_side) &
            ! Fill board which species numbered from 1 to 255
            board(i, j) = (i-1)*board_side + j
     end block
     total_n_classes = board_side_square
  endif

end subroutine fill_board
