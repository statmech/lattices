#!/bin/bash
usage () 
{
    cat << EOF
Usage: neutral_theory [options]

This script is a front-end to neutral_theory-classic

EOF

neutral_theory-classic

}

# Check the options
for i in "$@";
do
    if [[ $i == "-p" || $i == "--plot" || $i == "-D" || $i == "--plot-diff" ]]
    then
	pipe2gnuplot=1
    fi
done

if [[ -z $pipe2gnuplot ]]
then
    ./nt-forward "$@"
else
    ./nt-forward "$@" | gnuplot
fi

exit 0
