subroutine plot_init ()
  implicit none
  if (init_max_diversity) black = board_side_square
  if (init_min_diversity) black = N_COLORS_MIN_DIVERSITY
  call set_plot(board_side, black, .true.)
  ! draw the gliph while loading inital rectangles,
  ! but only if enough space
  if (board_side .gt. 10) then
     call set_plot_title("Loading rectangles...")
     call set_rect (board_side_half,   board_side_half+2, black)
     call set_rect (board_side_half+2, board_side_half,   black)
     call set_rect (board_side_half+2, board_side_half-2, black)
     call set_rect (board_side_half,   board_side_half-2, black)
     call set_rect (board_side_half-2, board_side_half-2, black)
  endif
  call plot_now()     
  block
    integer :: i, j
    do i = 1, board_side ! load rectangles
       do j = 1, board_side
          call set_rect (i, j, board(i, j))
       enddo
    enddo
  end block

  call plot_now()

  if (time_plot) board_aux = board
end subroutine plot_init
