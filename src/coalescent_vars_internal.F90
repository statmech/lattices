  ! Internal variables for managing the program

  !-----------------------
  ! Model specific
  !----------------------
  integer, allocatable :: board(:,:) ! Record walkers indexes, not species!
!  integer, allocatable :: available_neighbours(:,:)
  integer :: step, time
  integer :: x, y, xnew, ynew, xmod, ymod, xmodnew, ymodnew
  integer :: n_walkers     
  ! index of randomly chosen walker in walkers array (during main loop)
  integer :: walker_index
  ! ID of the last walker who was in certain node
  integer :: neighbour_index
  ! Saves info about walkers
  ! It will be continously rearranged so alive walkers
  ! be contiguous at the begining
  ! The program talks in terms of the index of every walker
  ! which is changing continouly. Without access to the field,
  ! not known which index has certain ID 
  type walker
     integer :: id     ! unique and pervasive identificator for each walker
     integer :: x      ! x-pos of walker now
     integer :: y      ! y-pos of walker now
     integer :: parent ! point to her mother, if coalescence happens
     integer :: date   ! when coalescence happens 
  end type walker
  type(walker), allocatable :: walkers(:)
  type(walker) :: walker_flag

  type indexes_list
     integer :: n_indexes
     integer :: list_size
     integer, allocatable :: list(:)
  end type indexes_list
  type(indexes_list), allocatable :: board_of_equal_module(:,:)

  ! After main loop, PARENT field is sorted by walker ID here 
  integer, allocatable :: walkers_parents(:)

  ! Comodities
  !---------------------
  ! Constant values
  integer :: board_side_square         ! Avoiding usual maths
  integer :: board_side_half
  character (len=20) :: rid            ! random id
  ! Variable values
  integer, allocatable :: flag(:)      ! A flag to use when allocating
  integer :: members_array_size
  character (len=MAX_FILENAME_BASE_LENGTH) :: filename_base

  !----------------------
  ! Implementation specific
  !----------------------

  ! Show debug messages
  logical :: verbose = .false.

  ! Measure mode
  !--------------
  integer :: n_scales
  integer, allocatable :: scales(:) 
  type scaled_observable
     integer, allocatable :: p(:)
  end type scaled_observable


  integer :: n_samples
  integer :: life_size
  integer, allocatable :: members(:)   ! # of specimens of some class
  integer, allocatable :: diversity(:)
  integer, allocatable :: sad(:)
  type(scaled_observable), allocatable :: scaled_sad(:)
  integer, allocatable :: sar(:,:)    ! 2-indexes for more protection
  integer, allocatable :: beta(:,:) ! ...against overflow
  integer, allocatable :: life(:)

  logical :: measure_option=.false., not_measure_option=.false.

  ! For clusters's measuring
  integer, allocatable :: surrounding(:,:)       
  integer, allocatable :: surrounding_found(:,:) 
  integer, allocatable :: clusters(:)
  integer, allocatable :: classes_found(:)

  ! Pointer procedures
  procedure(random_neighbour_pbc), pointer :: &
       random_neighbour => null()
  procedure(init_board_absolute), pointer :: &
       init_board                    => null()
  procedure(remove_walker_from_board_absolute), pointer :: &
       remove_walker_from_board      => null(), &
       look4neighbour_in_board       => null(), &
       refresh_walker_index_in_board => null(), &
       include_walker_in_board       => null()

  !----------------
  ! Results
  !----------------
  real (kind=8), allocatable :: array_aux_real(:,:)
  integer :: clad_unit

  !----------------------------------------
  ! Command line options (getopt module)
  !----------------------------------------
  character(len=100)  :: options
  type(option), allocatable :: longopts(:)
  character           :: optchar
  character (len=500) :: optarg
  integer             :: arglen, stat, offset, remain
  logical             :: err = .true.
