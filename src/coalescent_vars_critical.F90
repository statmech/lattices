  ! Variables which control execution of the program

  !--------------------------
  ! Model variables
  !--------------------------

  ! Lineal dimension of the square lattice (# of nodes = board_side**2)
  integer :: board_side = 20
  ! Number of nearest neighbours
  integer :: n_neighbours  = 8 ! values: 4, 8, 24
  ! Boundary Conditions
  logical :: boundary_closed = .false.
  logical :: boundary_pbc = .true.
  logical :: boundary_infinite =.false.
  ! Specify ratio of creation of new species (at time units)
  real    :: evolution_rate = 2E-6 

  !--------------------------
  ! Implementation variables
  !--------------------------

  ! Choose mode
  !--------------
  logical :: measure_mode   = .true.

  ! Measure mode
  !--------------
  integer :: total_n_samples = 1000
  integer :: anticipate_results_every_n_samples = 10000

  logical :: is_measure_diversity = .true.
  logical :: is_measure_sad       = .true.
  logical :: is_measure_sar       = .true.
  logical :: is_measure_beta      = .true.
  logical :: is_measure_clusters  = .true.
  logical :: is_measure_life      = .true.
  logical :: is_measure_cladogram = .true.

  integer :: max_n_cladograms  = -1 

#define IMPLEMENTATION "dual"

! Select name for the files and dirs
#define LIFE_NAME "life"
#define DIVERSITY_NAME "diversity"
#define RANKING_NAME "ranking"
#define SAD_NAME "sad"
#define SAR_NAME "sar"
#define BETA_NAME "beta"
#define CLUSTERS_NAME "clusters"
#define CLAD_NAME "clad"

#define RESULTS_DIR    "results"
#define SET_DIR(dir) RESULTS_DIR//"/"//dir
#define LIFE_DIR      SET_DIR(LIFE_NAME)
#define DIVERSITY_DIR SET_DIR(DIVERSITY_NAME)
#define RANKING_DIR   SET_DIR(RANKING_NAME)
#define SAD_DIR       SET_DIR(SAD_NAME)
#define SAR_DIR       SET_DIR(SAR_NAME)
#define BETA_DIR      SET_DIR(BETA_NAME)
#define CLUSTERS_DIR  SET_DIR(CLUSTERS_NAME)
#define CLAD_DIR      SET_DIR(CLAD_NAME)

#define PLOT_SCRIPT      "nt-plot_data.sh"
#define LOGBIN_SCRIPT    "nt-log-binning.awk"
#define GNUCLAD          "gnuclad"
#define CLAD_LOG_SCALE   "nt-time2gnuclad.awk"
#define CLAD_CONFIG_PATH "clad.conf"

#define DATA_EXTENSION    ".sal"
#define GNUPLOT_EXTENSION ".gnu"
#define GRAPH_EXTENSION   ".png"
#define BINNED_EXTENSION  ".binned"
#define CSV_EXTENSION     ".csv"
#define CLAD_EXTENSION    ".svg"

#define MAX_PATH_LENGTH 255
#define MAX_FILENAME_BASE_LENGTH 80

#define MAX_N_CLADOGRAMS_DEFAULT 5
