integer function init_clad_files(sample)
  implicit none
  integer, intent(in) :: sample
  character (len=MAX_PATH_LENGTH) :: path, clad_data_file
  character (len=30) :: sample_text

  write (sample_text,  '(i10)') sample
  sample_text  = adjustl(sample_text)

  path = full_path_except_ext(CLAD_DIR, filename_base, sample_text)
  clad_data_file   = trim(path) // CSV_EXTENSION
  open(newunit = init_clad_files, &   
       file    = clad_data_file, &
       action  = "WRITE")
  
endfunction init_clad_files

subroutine cladogram_newspecie(unit_n, specie, date)
  implicit none
  integer, intent(in) :: unit_n, specie, date
  character (len=30) :: text1, text2
  
  write (text1,  '(i10)') specie 
  text1  = adjustl(text1)
  write (text2,  '(i10)') date 
  text2  = adjustl(text2)
  
  write(unit_n, '(a)'), 'N,' // trim(text1) // ',#f00,,' &
       // trim(text2) // ',,,,'
  
end subroutine cladogram_newspecie
  
subroutine cladogram_coalescence(unit_n, child, parent, date)
  implicit none
  integer, intent(in) :: unit_n, child, parent, date
  character (len=30) :: text1, text2, text3
  
  write (text1,  '(i10)') child 
  text1  = adjustl(text1)
  write (text2,  '(i10)') parent 
  text2  = adjustl(text2)
  write (text3,  '(i10)') date 
  text3  = adjustl(text3)
  
  write(unit_n, '(a)'), 'N,' // trim(text1) //',#000,' &
       // trim(text2) // ',' // trim(text3) // ',,,,'    
  
end subroutine cladogram_coalescence

subroutine draw_cladogram (sample)
  implicit none
  integer, intent(in) :: sample
  character (len=MAX_PATH_LENGTH) :: path, clad_data_file, clad_datalog_file, clad_graph_file
  character (len=30) :: sample_text

  write (sample_text,  '(i10)') sample
  sample_text  = adjustl(sample_text)

  path = full_path_except_ext(CLAD_DIR, filename_base, sample_text)
  clad_data_file    = trim(path) // CSV_EXTENSION
  path = full_path_except_ext(CLAD_DIR, filename_base, trim(sample_text) // "_log")
  clad_datalog_file = trim(path) // CSV_EXTENSION
  clad_graph_file   = trim(path) // CLAD_EXTENSION  

  ! Create file with time in logscale
  call execute_command_line(&
       BINDIR // "/" &
       // CLAD_LOG_SCALE &
       // " " // clad_data_file &
       // " > " // clad_datalog_file)

  ! Create cladogram
  call execute_command_line(GNUCLAD // " " &
       // trim(clad_datalog_file)   // " " &
       // trim(clad_graph_file)     // " " &
       // PKGDATADIR // "/" // CLAD_CONFIG_PATH)
  
endsubroutine draw_cladogram
