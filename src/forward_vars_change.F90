subroutine parse_arguments_and_change_vars ()

  ! FIRST: Define available command line arguments

  options = "hvl:HS:Ee482cr:s:n:pd:DVm:M:" ! HEY!!! CHANGE MAX!!!
  !          123 45 6789012 3 4 56 789 0  *20

  ! HEY!!! CHANGE ME!!!
#define NUMBER_OF_OPTIONS 20
    allocate(longopts(NUMBER_OF_OPTIONS))
#undef NUMBER_OF_OPTIONS

  longopts = &
       (/    &
       ! Content:
       ! Long name, does require an argument?, short-name, decription, optarg
       option("help",    .false., "h",  &
       "Print this help and exit", ""), &

       option("version", .false., "v",  &
       "Show version and exit", ""),    &

       option("side",    .true.,  "l",  &
       "Side of the square lattice. Default is 20", "<side>"),       &

       option("harmony", .false., "H", &
       "Look for harmony-mode", ""), &

       option("stop-look",    .true.,  "S",  &
       "Time to stop look4harmony mode. Default is 10*4", "<time>"),       &

       option("max-diversity", .false., "E", &
       "Initial conditions are maximal diversity", ""), &

       option("min-diversity", .false., "e", &
       "Initial conditions are minimal diversity. Default", ""), &

       option("4-neighbours", .false., "4", &
       "Only the four nearest neighbours have relevance", ""), &

       option("8_neighbours", .false., "8", &
       "Only the eight nearest neighbours have relevance. Default", ""), &

       option("24-neighbours", .false., "2", &
       "Only the twentyfour nearest neighbours have relevance", ""), &

       option("closed", .false., "c", &
       "Do not use periodic boundary conditions (some statistics will lie)", ""), &

       option("evolution-rate", .true., "r", &
       "Set to <rate> the evolution rate. Default is 0.01", "<rate>"), &

       option("saving-data-start", .true., "s", &
       "Save data from <time> to the end of the simulation. " // &
       "Default is 1E+4", "<time>"), &

       option("samples", .true., "n", &
       "Number of system's snapshots to measure." // &
       "Default is 1E+3", "<times>"), &


       option("delay", .true., "d", &
       "In time-plot mode, system is refreshed with every <time>; default is 1. " // &
       "In look for harmony mode, a max of <time> measures are taken between any power of ten units of time; default is 1E+3. " // &
       "In measure mode, measures are taken every <time>; default is 50." &
       , "<time>"), &

       option("plot", .false., "p", &
       "It's the time-plot mode. Display the lattice each unit of time", ""), &

       option("plot-diff", .false., "D", &
       "It's the verbose/diff-plot mode. Display the lattice every " // &
       "monte-carlo step in which the lattice results in a different state", ""), &

       option("measure",    .false., "m",  &
       "Take measures of specified observables. The list is (beta life " // &
       "sad clusters diversity). Only three first characters are checked. " // &
       "Write -m before every observable you are interested.", "<text>"), &

       option("not-measure",    .false., "M",  &
       "Idem than --measure, but all observables are measured except the specified ones.", "<text>"), &

       option("verbose", .false., "V", &
       "Print messages showing status of the program.", "") &       
        /)
  ! SECOND: Parse the command line arguments and modify critical vars
  do
     call getopt(options, longopts, optchar, optarg, arglen, &
          stat, offset, remain, err)
     if (stat /= 0) then
        ! Success parsing all options
        if (stat == 1) then
           ! More options yet?
           if (remain /= 0) then
              call print_help()
              stop
           else
              exit
           endif
        else ! Error parsing options
           call print_help()
           stop
        endif
     endif
     ! "hvl:Mm48ir:s:D:t:pd:VP"
     select case (optchar)
        case ("h")
           call print_all_opts(longopts)
           stop
        case ("v")
           print *, "This is non-numbered development version yet."
           stop
        case("l")
           read (optarg, '(I10)') board_side
        case("H")
           look4harmony_mode = .true.
           measure_mode = .false.
           plot_mode = .false.
        case("S")
           read (optarg, '(I10)') look4harmony_stop
        case ("E")
           init_max_diversity = .true.
           init_min_diversity = .false.
        case ("e")
           init_min_diversity = .true.
           init_max_diversity = .false.
        case ("4")
           n_neighbours = 4
        case ("8")
           n_neighbours = 8
        case ("2")
           n_neighbours = 24
        case ("c")
           boundary_pbc    = .false.
           boundary_closed = .true.
        case ("r")
           read (optarg, '(G10.10)') evolution_rate
        case ("s")
           read (optarg, '(I10)') measure_start
        case ("n")
           read (optarg, '(I10)') total_n_samples
        case ("p")
           time_plot = .true.
           plot_mode = .true.
           look4harmony_mode = .false.
           measure_mode = .false.
        case ("d")
           read (optarg, '(I10)') delay_aux
        case ("D")
           diff_plot = .true.
           plot_mode = .true.
           look4harmony_mode = .false.
           measure_mode = .false.
        case("M")
           not_measure_option = .true.
           block
             character (len=10) :: pattern
             read (optarg, '(A3)') pattern
             select case (pattern)
             case ("sar")
                is_measure_sar = .false.
             case ("sad")
                is_measure_sad = .false.
             case ("bet")
                is_measure_beta = .false.
             case ("div")
                is_measure_diversity = .false.
             case ("lif")
                is_measure_life = .false.
             case ("clu")
                is_measure_clusters = .false.
             case default
                print *, 'Error parsing observable starting by: "', trim(pattern), '"'
                stop
             end select
           end block
        case("m")
           measure_option = .true.
           block
             character (len=10) :: pattern
           read (optarg, '(A3)') pattern
           select case (pattern)
           case ("sar")
              ! Trick: We will use negation later
              is_measure_sar = .false.
           case ("sad")
              is_measure_sad = .false.
           case ("bet")
              is_measure_beta = .false.
           case ("div")
              is_measure_diversity = .false.
           case ("lif")
              is_measure_life = .false.
           case ("clu")
              is_measure_clusters = .false.
           case default
              print *, 'Error parsing observable starting by: "', trim(pattern), '"'
              stop
           end select
         end block
        case ("V")
           verbose = .true.
     end select
  enddo
  deallocate(longopts)

end subroutine parse_arguments_and_change_vars
