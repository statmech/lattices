  function probability_of_flipping(tau, sigma, nn_diff)
    implicit none
    integer, intent(in) :: tau, sigma, nn_diff
    integer :: probability_of_flipping
    if (tau .eq. sigma) then
       probability_of_flipping = &
            (1+eps)*nn_diff/((1+eps)*nn_diff+(1-eps)*(1-nn_diff))
    else
       probability_of_flipping = &
            (1-eps)*nn_diff/((1-eps)*nn_diff+(1+eps)*(1-nn_diff))
    endif
  end function probability_of_flipping



! RANDOM_NEIGHBOUR_WITH_PREFERENCE
!
!  f=(1+eps)*x/((1+eps)*x+(1-eps)*(1-x))
!  else:
!  f=(1-eps)*x/((1-eps)*x+(1+eps)*(1-x))
!
!
!
! Description:
!
! This program reproduces the voter model standing on the following notes:
!  * Square lattice.
!  * Every iteration, a random point of the lattice is deleted.
!  * That point takes the value of one neighbour (with equal probability) 
!  * Border effects: nodes at the borders of the lattice have less neighbours
!
! This program is easily extensible to more considered neighbours;
! however, only 4 and 8 have been coded until now.

!----------------- SETTING PARAMETERS ---------------

!------------------------------------------------------------
#define      LEFT_BORDER x == 1
#define     RIGHT_BORDER x == board_side

#define Y_DECR y-1+board_side * ((board_side+1-y)/board_side) 
#define Y_INCR y+1-board_side * (y/board_side) 

#define UP         new_member_index = board(x  , Y_INCR)
#define DOWN       new_member_index = board(x  , Y_DECR)
#define LEFT       new_member_index = board(x-1, y  )
#define RIGHT      new_member_index = board(x+1, y  )
#define UP_RIGHT   new_member_index = board(x+1, Y_INCR)
#define DOWN_RIGHT new_member_index = board(x+1, Y_DECR)
#define UP_LEFT    new_member_index = board(x-1, Y_INCR)
#define DOWN_LEFT  new_member_index = board(x-1, Y_DECR)

subroutine random_neighbour_index_preference(x, y, new_member_index)
  implicit none
  integer, intent(in) :: x, y
  integer, intent(out) :: new_member_index

  integer :: my_specie = board (x, y)
  integer :: nn_different = 0
  INTEGER :: magic_n
  INTEGER :: i_ran ! Function used for getting random numbers

  !===========================================================================
  ! Note: "magic_n" is used for assigning a neighbour to the current state.
  !       This takes a random value from 1 to # of neighbours, and then
  !       it is mapped into some neigbour through a "hardcoded" CASE.
  !
  !       Adventages: * Trivial extensibility of the number of neighbours
  !                   * You will go out of the CASE quickly:
  !                     If only n chosen neighbours, one of them will be
  !                     assigned before reaching the n+1 CASE, although
  !                     other instances of the CASE are also available
  !                     "further" if you chose a # of neighbours gt n.
  !============================================================================

        ! ------------------------------ Strange case: left side
  IF (LEFT_BORDER) THEN

        ! Diagram of chosen coordinates
        !
        !    | 1 4
        !    | x 2  
        !    | 3 5

     ! Count how many neighbours are different to you

     if (board(x, Y_INCR) /= my_specie) nn_different = nn_different + 1
     if (board(x+1, y)    /= my_specie) nn_different = nn_different + 1
     if (board(x, Y_DECR) /= my_specie) nn_different = nn_different + 1
     if (n_neighbours == 8) then
          if (board(x+1, Y_INCR) /= my_specie) nn_different = nn_different + 1
          if (board(x+1, Y_DECR) /= my_specie) nn_different = nn_different + 1
     endif

     if (probability_of_flipping (+1, my_specie, nn_different) .ge. ran_u()) then
        if (my_specie .eq. 1) then
           new_member_index = 2
        else
           new_member_index = 1
        endif
     else
        new_member_index = my_specie
     endif


     ! ------------------------------ Strange case: right side
  ELSE IF (RIGHT_BORDER) THEN
        ! Diagram of chosen coordinates
        !
        !    5 1 |
        !    3 x |  
        !    4 2 |

     ! Count how many neighbours are different to you

     if (board(x, Y_INCR) /= my_specie) nn_different = nn_different + 1
     if (board(x-1, y)    /= my_specie) nn_different = nn_different + 1
     if (board(x, Y_DECR) /= my_specie) nn_different = nn_different + 1
     if (n_neighbours == 8) then
        if (board(x-1, Y_DECR) /= my_specie) nn_different = nn_different + 1
        if (board(x-1, Y_INCR) /= my_specie) nn_different = nn_different + 1
     endif

     if (probability_of_flipping (+2, my_specie, nn_different) .ge. ran_u()) then
        if (my_specie .eq. 1) then
           new_member_index = 2
        else
           new_member_index = 1
        endif
     else
        new_member_index = my_specie
     endif

  ELSE
     ! Get a magic number
     magic_n = i_ran(n_neighbours)

     select case (magic_n)

           ! Diagram of chosen coordinates
           !
           !   5 1 6
           !   4 x 2
           !   8 3 7

     case (1)
        UP
     case (2)
        RIGHT
     case (3)
        DOWN
     case (4)
        LEFT
     case (5)
        UP_LEFT
     case (6)
        UP_RIGHT
     case (7)
        DOWN_RIGHT
     Case (8)
        DOWN_LEFT
     end SELECT

  ENDIF

END SUBROUTINE random_neighbour_index_preference

#undef      LEFT_BORDER
#undef     RIGHT_BORDER

#undef LEFT
#undef DOWN
#undef RIGHT
#undef UP
#undef UP_RIGHT  
#undef DOWN_RIGHT
#undef UP_LEFT   
#undef DOWN_LEFT 
