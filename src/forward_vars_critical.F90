  ! Variables which control execution of the program

  !--------------------------
  ! Model variables
  !--------------------------

  ! Lineal dimension of the square lattice (# of nodes = board_side**2)
  integer :: board_side = 20
  ! Number of nearest neighbours
  integer :: n_neighbours  = 8
  ! Periodic Boundary Conditions or non-permeable borders
  logical :: boundary_pbc    = .true.
  logical :: boundary_closed = .false.
  logical :: boundary_infinite = .false. ! Impossible!, only for compatibility
  ! Specify ratio of creation of new species (at time units)
  real    :: evolution_rate = 2E-6 

  !--------------------------
  ! Implementation variables
  !--------------------------

  ! Initial conditions
  !--------------------
  logical :: init_max_diversity = .false. ! Every node is a different class
  logical :: init_min_diversity = .true.  ! Every node has the same class

  ! Choose mode
  !--------------
  logical :: measure_mode = .true.
  logical :: look4harmony_mode = .false.
  logical :: plot_mode = .false.

  ! Measure mode
  !--------------
  ! When to start taking measures (at time units)
  ! (if it is not a multiple of saving_data_delay, measuring
  ! will start at the next time it is)
  integer :: measure_start = 10**4
  ! How often to take data (at time units).
  integer :: measure_delay = 50
  ! Number of snapshots to measure
  integer :: total_n_samples = 10**3
  integer :: anticipate_results_every_n_samples = 10000

  ! NOTE: End of simulation will be at
  !   start_saving_data + total_n_samples * saving_data_delay
  ! except precisions stated above

  logical :: is_measure_diversity = .true.
  logical :: is_measure_sad       = .true.
  logical :: is_measure_sar       = .true.
  logical :: is_measure_beta      = .true.
  logical :: is_measure_life      = .true.
  logical :: is_measure_clusters  = .true.

  ! Look 4 harmony mode
  !---------------------
  ! Stop measuring at this TIME
  integer :: look4harmony_stop = 10**5
  ! Take measures every 10**(n/decade_div), n natural
  integer :: decade_div = 500

  ! Plot mode
  !-----------
  ! Plot every time_plot_delay units of TIME 
  logical :: time_plot = .false.
  integer :: time_plot_delay = 1
  ! Plot always that some STEP changed the board
  logical :: diff_plot = .false.

#define IMPLEMENTATION "voter"

! Select name for the files
! Select name for the files and dirs
#define LIFE_NAME "life"
#define DIVERSITY_NAME "diversity"
#define RANKING_NAME "ranking"
#define SAD_NAME "sad"
#define SAR_NAME "sar"
#define BETA_NAME "beta"
#define CLUSTERS_NAME "clusters"

#define RESULTS_DIR    "results"
#define SET_DIR(dir) RESULTS_DIR//"/"//dir
#define LIFE_DIR      SET_DIR(LIFE_NAME)
#define DIVERSITY_DIR SET_DIR(DIVERSITY_NAME)
#define RANKING_DIR   SET_DIR(RANKING_NAME)
#define SAD_DIR       SET_DIR(SAD_NAME)
#define SAR_DIR       SET_DIR(SAR_NAME)
#define BETA_DIR      SET_DIR(BETA_NAME)
#define CLUSTERS_DIR  SET_DIR(CLUSTERS_NAME)

#define PLOT_SCRIPT   "nt-plot_data.sh"
#define LOGBIN_SCRIPT "nt-log-binning.awk"

#define DATA_EXTENSION    ".sal"
#define GNUPLOT_EXTENSION ".gnu"
#define GRAPH_EXTENSION   ".png"
#define BINNED_EXTENSION  ".binned"

#define MAX_PATH_LENGTH 255
#define MAX_FILENAME_BASE_LENGTH 80


! # of main colors in min-diversity mode
#define N_COLORS_MIN_DIVERSITY 14
